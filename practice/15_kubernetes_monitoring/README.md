# URL

[Grafana](https://grafana.kuber.atp-fivt.org/?orgId=1)

## Авторизация

```bash
```

## Проверка шаблона

```bash
helm repo add prometheus-community https://prometheus-community.github.io/helm-charts
helm repo update
helm -n monitoring upgrade --install  -f values.yaml mon prometheus-community/kube-prometheus-stack --wait --debug -n observability
```
