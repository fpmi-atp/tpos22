from setuptools import setup, find_packages
import os

here = os.path.abspath(os.path.dirname(__file__))

with open(os.path.join(here, "README.md"), encoding="utf-8") as fh:
    long_description = "\n" + fh.read()

VERSION = '0.0.1'
DESCRIPTION = 'Simple Hello World package'

# Setting up
setup(
    name="helloworld",
    version=VERSION,
    author="Nikita Chestnov",
    author_email="<chestnov.nn@phystech.edu>",
    description=DESCRIPTION,
    long_description_content_type="text/markdown",
    long_description=long_description,
    packages=find_packages(),
    install_requires=[],
    entry_points={'console_scripts': [
        'helloworld = helloworld.main:print_hi_console'
    ]},
    keywords=['python', 'simple', 'hello world'],
    classifiers=[
        "Development Status :: 1 - Planning",
        "Intended Audience :: Developers",
        "Programming Language :: Python :: 3",
        "Operating System :: OS Independent",
        "License :: OSI Approved :: MIT License",
    ]
)
