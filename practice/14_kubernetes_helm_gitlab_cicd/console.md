## Установка helm3

curl https://raw.githubusercontent.com/helm/helm/main/scripts/get-helm-3 | bash

#3 Chart Postgres

helm repo add bitnami https://charts.bitnami.com/bitnami

helm pull bitnami/postgresql

helm install my-release bitnami/postgresql


## Конфиг для доступа к кластеру

```yml
apiVersion: v1
clusters:
- cluster:
    certificate-authority-data: LS0tLS1CRUdJTiBDRVJUSUZJQ0FURS0t..
    server: https://93.175.29.167:16443
  name: microk8s-cluster
contexts:
- context:
    cluster: microk8s-cluster
    namespace: ns
    user: user
  name: microk8s
current-context: microk8s
kind: Config
preferences: {}
users:
- name: user
  user:
    token: T3ZsQi82OWJOSTZ...
```

## Авторизация в кластере через Gitlab

kubectl config set-cluster mfti --server="https://93.175.29.167:16443"

kubectl config set clusters.k8s.certificate-authority-data "LS0tLS1CRUdJTiBDR...."

kubectl config set-credentials gitlab --token="T3ZsQi8..."

kubectl config set-context default --cluster=mfti --user=gitlab

kubectl config use-context default

kubectl config set-context --current --namespace $YOUR_NAMESPACE


## Секреты для доступа к registry

kubectl --namespace ${KUBERNETES_NAMESPACE} delete  --ignore-not-found=true secret image-secret

kubectl --namespace ${KUBERNETES_NAMESPACE} create secret docker-registry image-secret --docker-server=${CI_REGISTRY} --docker-username=${CI_DEPLOY_USER} --docker-password=${CI_DEPLOY_PASSWORD}

