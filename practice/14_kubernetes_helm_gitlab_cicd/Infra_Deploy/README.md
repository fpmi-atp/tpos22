# Infra Deploy

Проект для разворачивания инраструктурных сервисов.

## Содержимое

* [PostgreSQL](helm/charts/postgresql/)

* [Redis](helm/charts/redis/)

* [RabbitMQ](helm/charts/rabbitmq/)

* [Kafka](helm/charts/kafka/)

* [CMAK](helm/charts/cmak/)



## Тестирование и установка

```bash
helm template infra . --debug 
```

```bash
helm upgrade --install infra . --debug --wait 
```
